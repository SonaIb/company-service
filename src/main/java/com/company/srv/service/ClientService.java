package com.company.srv.service;

import com.company.srv.dto.ClientDto;

public interface ClientService {


    ClientDto getClientById(Long ID);
    void deleteClient(Long clientId);
    ClientDto create(ClientDto clientDto);
    ClientDto save (ClientDto clientDto);
}
