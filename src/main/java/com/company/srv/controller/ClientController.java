package com.company.srv.controller;

import com.company.srv.dto.ClientDto;
import com.company.srv.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //rest api-dir
@RequestMapping(value = "/client")
@RequiredArgsConstructor//injection dependency autowired eyni funsksiya icra edir
public class ClientController {
//client/id?1=>query param
//client/id/1 => pastparam
    private final ClientService clientService;

    @GetMapping("/id/{id}")
    public ClientDto getClientById(@PathVariable Long id){

        return clientService.getClientById(id);
    }
}
