package com.company.srv.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor //deafult konstruktor
@AllArgsConstructor // konstruktorunu yaradir parametrli
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClientDto{
     Long id;
     String name;
     String surname;
     String gender;
     String phone;
     String nationality;
}
